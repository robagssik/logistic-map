import numpy
import matplotlib.pyplot as plt
x0=.4
mu=0
#print("EQ=%f"%(1-1/mu))
print("0: ","%f"%(x0))

par=[]
par.append(mu)
orbit=[]
orbit.append(x0)
orbtail=[]
partail=[]

parmax=50000

for j in range (0,parmax):
    x0=.4
    mun=4*j/parmax
    for i in range(0,1000):
        #print("%d: "%i," %d"%j,"%f"%(mun)," %f"%(x0))
        xn=(mun*x0*(1-x0))
        par.append(mun)
        orbit.append(xn)
        if i>500:
            orbtail.append(xn)
            partail.append(mun)
        x0=xn

        
print("%d: "%i,"%f"%(x0))
plt.scatter(partail,orbtail,s=0.01,marker='.')
#plt.plot(orbit,'ro')
plt.show()
print("Done")
